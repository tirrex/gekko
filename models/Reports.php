<?php

namespace app\models;

use Yii;
use yii\base\ErrorException;
use app\models\Reports;
use yii\helpers\Json;
/**
 * Класс модели для таблицы "reports", содержащей данные о загруженных файлах.
 *
 * @property integer $id - ид загруженного файла
 * @property string $filename - имя загруженного файла
 * @property string $realfilename - имя загруженного файла, под которым он хранится на сервере 
 */
class Reports extends \yii\db\ActiveRecord
{
    /**
     * возврат имени таблицы данной модели
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * правила проверки полей модели
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //обязательные к заполнению
            [['filename', 'realfilename'], 'required'],
            //файл должен быть уникальным - чтобы исключить повторную загрузку
            ['realfilename', 'unique'],
            //формат - string, максимальная длина 50 символов
            [['filename', 'realfilename'], 'string', 'max' => 50],
        ];
    }
    
    /**
     * функция разбора загруженного html и формирования файла, 
     * содержащего необходимые данные для построения  графика в формате json
     * @return boolean результат обработки
     */
    
    public function parseReportFile(){
        
        //результат операции
        $OperationRezult=true;
        //увеличиваем время работы скрипта
        set_time_limit(300);
  
        //объект для импорта html и разбора DOM
        $UploadedHTMLDoc = new \DOMDocument();
        try {
            if(!$UploadedHTMLDoc->loadHTMLFile(Yii::$app->params['uploaddir'].$this->realfilename)) {

                libxml_clear_errors();
                return false;
               
            }
        }
        catch (ErrorException $e) {
            return false;
        }
        //выборка элементов html 
       //body
        $Body = $UploadedHTMLDoc->getElementsByTagName( "body" );
        //блоки div
        if($Body->item(0)){
             $Divs = $Body->item(0)->getElementsByTagName( "div" );   
        }
        else {
            return false;
        }
        if($Divs->item(0)){
            $InnerDivs = $Divs->item(0)->getElementsByTagName( "div" );     
        }
        else {
            return false;
        }
        if($InnerDivs->item(0)){
             $ReportTitle = $InnerDivs->item(0)->nodeValue;
        }
        else {
            return false;
        }
       
        //таблицы 
        $Tables = $UploadedHTMLDoc->getElementsByTagName( "table" );
        //собираем общую информацию о массиве данных для шапки таблицы

        //таблица с транзакциями
        if($Tables->item(0)) {
              $TransactionTable = $Tables->item(0);
        }
        else {
            return false;
        }
        
        //все строки таблицы с транзакциями
        if($TransactionTable) {
            $TransactionRows = $TransactionTable->getElementsByTagName( "tr" );
        }
        else {
            return false;
        }
        
        if($TransactionRows->length < 2) return false;
      
        //ячейки первой строки
        if($TransactionRows->item(0)) {
              $CurrentRowCells = $TransactionRows->item(0)->getElementsByTagName("td");
        }
        else {
            return false;
        }
      
        //заполняем заголовок графика          
        $this->reporttitle = '<h3>'.$ReportTitle.'</h3>'; 
        if ($CurrentRowCells->length ) {
            for ($i=0;$i<$CurrentRowCells->length;$i++) {
                if ($i == $CurrentRowCells->length-1) {
                    $this->reporttitle.= $CurrentRowCells->item($i)->nodeValue;
                }
                else {
                    $this->reporttitle.= $CurrentRowCells->item($i)->nodeValue.', ';
                }
          
            }
        }    
        else {
            return false;
        }
       // $this->save();
        //сохраняем модель
        if ( !$this->validate() || !$this->save() ) return false;
        //массив для накопления данных о транзакциях 
        $TransactionData=[];
        //имя файла для записи данных о транзакциях
        $TransactionFileName=Yii::$app->params['datadir'].'data'.$this->id.'.data';
        //переменная для хранения текущего баланса 
        $CurrentBalance = 0;
        //перебор строк в таблице транзакций 
       
        for ($i=0;$i<$TransactionRows->length;$i++) {
            //ячейки текущей строки
            if ($TransactionRows->item($i)){
           
            
                $CurrentRowCells = $TransactionRows->item($i)->getElementsByTagName("td");
            //если ячейки обнаружены      
                if (gettype($CurrentRowCells->item(1))==='object' && gettype($CurrentRowCells->item(2))==='object') {
                    // var_dump($CurrentRowCells)->item(1).'<br>';
                    // var_dump($CurrentRowCells)->item(2).'<br>';
                    // var_dump($CurrentRowCells)->item(13).'<br>';
                    // var_dump($CurrentRowCells)->item(4).'<br>';
                
                    //величина изменения баланса
                    $CurrentProfit = 0;
                    //дата и время транзакции
                    $DateTime = trim($CurrentRowCells->item(1)->nodeValue);
                    
                    //проверяем соответствие шаблону поле даты,если нет - возврат к началу цикла                                 
                    $DateTimePattern = '/\d{4}\.\d{2}.\d{2}\s\d{2}\:\d{2}(\:\d{2}|)/';
                    
                    if (preg_match ($DateTimePattern,$DateTime)){
                         $TransactionData['time'][] = [$DateTime];
                    }
                    else {
                        continue;
                    }
                    //тип операции                
                    $TypeOfDeal = trim($CurrentRowCells->item(2)->nodeValue);
                    //заполняем массив данными
                    $TransactionData['type'][] = [$TypeOfDeal];
                    // $TransactionData['order'][] = [$CurrentRowCells->item(3)->nodeValue];
                    // $TransactionData['volume'][] = [$CurrentRowCells->item(4)->nodeValue];
                    // $TransactionData['price'][] = [$CurrentRowCells->item(5)->nodeValue];
                    // $TransactionData['sl'][] = [$CurrentRowCells->item(6)->nodeValue];
                    // $TransactionData['tp'][] = [$CurrentRowCells->item(7)->nodeValue];
                    //величина изменения баланса  
        
                                         
                        switch ($TypeOfDeal) {
                          case 'buy':
                               if( gettype($CurrentRowCells->item(13))==='object'){
                                   $CurrentProfit = (float)str_replace(" ","",$CurrentRowCells->item(13)->nodeValue);
                               }
                               else {
                                     return false;
                               }                        
                              break;
                          case 'sell':
                               if( gettype($CurrentRowCells->item(13))==='object'){
                                   $CurrentProfit = (float)str_replace(" ","",$CurrentRowCells->item(13)->nodeValue);
                               }
                               else {
                                     return false;
                               }                        
                              break;
                          case 'balance':
                                
                                    
                                
                                if( gettype($CurrentRowCells->item(4))==='object'){
                                    $CurrentProfit = (float)str_replace(" ","",$CurrentRowCells->item(4)->nodeValue);
//                                    if ($DateTime == '2015.11.03 13:49:34')  {
//                                        var_dump($CurrentProfit);
//                                        die();
//                                    }
                                }
                                else {
                                     return false;
                                }  
                              break;
                          default:
                              $CurrentProfit = 0;
                              break;
                        }
                    
                    
                    //баланс нарастающим итогом  
                    $CurrentBalance = $CurrentBalance + $CurrentProfit;
                     //заполняем массив данными
                    $TransactionData['balance'][] = [$CurrentBalance];
                    $TransactionData['profit'][] = [$CurrentProfit]; 


               } 

            }
            else {
                return false;
            }
        }
        //вывод информации о транзакциях в файл 
        if ($TransactionData){
   
            try{
                @file_put_contents($TransactionFileName, json_encode($TransactionData));
            } catch (Exception $ex) {
                return false;
            }
        }
        else {
            return false;
        }
        return true;
    }
    
    /**
     * задает наименования атрибутов
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Имя файла',
            'realfilename' => 'Имя файла на сервере',
            'statistic' => 'Статистика',
            'reporttitle' => 'Наименование отчета'
        ];
    }

    /**
     * @inheritdoc
     * @return  активный запрос ReportsQuery, используемый этим AR классом.
     */
    public static function find()
    {
        return new ReportsQuery(get_called_class());
    }
}
