<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
/**
 * Класс модели для формы загрузки файла.
 *
 * @property ReportFile объект загруженного файла
 */
class UploadForm extends Model
{
    /**
     * @var ReportFile
     */
    public $ReportFile;
    //правила проверки полей ввода
    public function rules()
    {
        return [
            //файл должен быть выбран, допустимы расширения - htm,html
            [['ReportFile'], 'file', 'skipOnEmpty' => false, 'extensions' => ['htm','html']]
        ];
    }
    /**
     * проверка полученной формы загрузки, копирование файла из временного каталога в каталог
     * загрузок и генераия  имени по его md5 хешу
     * @return имя файла под которым он хранится на сервере
     */
    public function upload()
    {
        //если файл пустой
        $FileUploadState = true;
        if(!filesize($this->ReportFile->tempName)){
             return false;
        }
        //если введенные данные верны
        if ($this->validate()) {
            //генерируем md5 хеш - для использования в качестве имени файла
            $hash = md5_file($this->ReportFile->tempName);
            // копирование файла из временного каталога в каталог загрузок    
            $this->ReportFile->saveAs('uploads/' . $hash);
            if (!file_exists('uploads/' . $hash)) $FileUploadState = false;
           
        } else {
            //если в форму ввели неверные данные
            $FileUploadState = false;
        }
        
        return $FileUploadState?$hash:false;
                       
    }
}