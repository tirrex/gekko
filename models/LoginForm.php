<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm является моделью формы входа.
 *
 * @property User|null $user Это свойство только для чтения.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return Массив правил проверки.
     */
    public function rules()
    {
        return [
            // имя пользователя и пароль обязательные для заполнения поля
            [['username', 'password'], 'required'],
            // RememberMe должно быть логическим значением
            ['rememberMe', 'boolean'],
            // пароль проверяется методом validatePassword ()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Проверка пароля.
     * Этот метод служит для инлайн проверки пароля.
     *
     * @param string $attribute проверяемый атрибут
     * @param array $params дополнительные пары имя-значение, приведенные в правиле
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильное имя пользователя или пароль.');
            }
        }
    }

    /**
     * Вход пользователя, используя предоставленный имя пользователя и пароль.
     * @return boolean true когда пользователь успешно вошел в систему
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Поиск пользователя по имени - [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
    
    
     /**
     * задает наименования атрибутов
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [          
            'username' => 'Логин',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня'
        ];
    }
}
