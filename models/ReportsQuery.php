<?php

namespace app\models;

/**
 *
 * Это класс ActiveQuery для [[Reports]] - модели отчетов.
 *
 * @see Reports
 */
class ReportsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * все записи
     * @return Reports[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * одна запись
     * @return Reports|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
