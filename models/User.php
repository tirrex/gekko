<?php

namespace app\models;
/**
 * Класс модели пользователя (не ActiveRecord)
 *
 * @property string id id пользователя
 * @property string username логин
   @property string password пароль
   @property string authKey ключ авторизации
   @property string accessToken токен доступа
 * @property users array массив пользователей
 */
class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
     
    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin123!',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo123!',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];


    /**
     * поиск пользователя по id
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * поиск пользователя по токену доступа
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * поиск пользователя по имени пользователя
     *
     * @param string $username имя пользователя
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * получение id пользователя
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * получение ключа авторизации
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * проверка ключа авторизации
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * проеврка пароля
     *
     * @param string $password пароль для проверки
     * @return boolean если пароль действителен для текущего пользователя
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
