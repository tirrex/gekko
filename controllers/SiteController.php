<?php

namespace app\controllers;
use app\models\Reports;
use app\models\UploadForm;
use yii\data\ActiveDataProvider;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
/**
* Главный контроллер сайта
*/
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index','delete','view','download'],
                'rules' => [
                    [
                        'actions' => ['logout','index','delete','view','download'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],          
        ];
    }
     /**
     * Скачивание загруженного файла
     *
     * @return контент главной
     */
    public function actionDownload($id)
    {
        //поиск модели нужного отчета
        $ReportModel = new Reports;
        $ReportModel = $this->findModel($id);
              
        if (!$ReportModel){
             \Yii::$app->getSession()->setFlash('error', "Отчет не выбран или сессия истекла, пожалуйста, выберите Отчет для работы!");
             return $this->goHome();
        }
        $file = Yii::getAlias('@webroot').'/uploads/'.$ReportModel->realfilename;

        return Yii::$app->response->sendFile($file,$ReportModel->filename,['mimeType'=>'text/html']); 
    }
    /**
     * Создает и выводит главную страницу
     *
     * @return контент главной
     */
    public function actionIndex()
    {
        //выборка всех моделей загруженных файлов
        $AllReportsQuery = Reports::find();
        //экземпляр модели формы загрузки файла
        $UploadFormModel = new UploadForm();
        //данные для вывода списка всех загруженных файлов на главной
        $dataProvider = new ActiveDataProvider([
            'query' => $AllReportsQuery,
        ]);
        //если нажата кн. загрузки файла
        if (Yii::$app->request->isPost) {
            //получаем инфо о загруженном на сервер файле данных
            $UploadFormModel->ReportFile = \yii\web\UploadedFile::getInstance($UploadFormModel, 'ReportFile');
            //вызов ф-ции которая перемещает загруженный файл в соотв каталог для хранения и возвращает его имя
            //файл пока не удаляем на случай необходимости повторного разбора
            $NewFileName = $UploadFormModel->upload();
            //если сохранение прошло удачно
            //новый экземпляр модели отчета с данными
            $ReportModel = new Reports;
            if ($NewFileName) {
                
                //заполняем сведениями о загруженном файле
                $ReportModel->filename = $UploadFormModel->ReportFile->name;
                $ReportModel->realfilename = $NewFileName;                             
                //если проверка и сохранение новой модели удачны - импортирум данные из файла
                if ($ReportModel->validate()) {
                     if ($ReportModel->save() && $ReportModel->parseReportFile()){
                        //импорт данных из файла для построения графика
                        //данные будут хранится в формате json в отдельном каталоге на сервере
                      
                        //редирект на страницу просмотра графика
                        $this->redirect(['view','id' => $ReportModel->id]);
                    }    
                    else {              
                                          
                        \Yii::$app->getSession()->setFlash('error', "Ошибка: файл не содержит данных, или их формат неверен!",false );
                         $this->actionDelete($ReportModel->id);
                    }
                }    
                else {
                        \Yii::$app->getSession()->setFlash('error', "Ошибка: Файл уже был загружен или имеет неправильный формат!",false );
                         $this->actionDelete($ReportModel->id);
                }    
            }
            else {
                 \Yii::$app->getSession()->setFlash('error', "Ошибка: файл не удалось прочитать или он нулевого размера!",false );
            }
        }
        //формирование вида главной страницы
        return $this->render('index', [
            'model' => $UploadFormModel,
            'dataProvider' => $dataProvider,
        ]);           
    }
  
    /**
     * удаление загруженного отчета и всех сопутств-х файлов
     *
     */
    public function actionDelete($id)
    {
        //хранение статуса удаления файлов на диске
        $DeleteStatus = true;
        //поиск модели удаляемого отчета
        $ReportModel = Reports::findOne($id);
        //если нашли
        if ($ReportModel){      
            //удаляем файл с данными и загруженный исходный файл
            try {
                @unlink(Yii::$app->params['datadir'].'data'.$ReportModel->id.'.data');         
                @unlink(Yii::$app->params['uploaddir'].$ReportModel->realfilename);
            } catch (Exception $ex) {
                $DeleteStatus=false;
            }
            //если файлы удалены успешно, удаляем запись о загруженном файле из базы данных 
            if ($DeleteStatus) {
                $ReportModel->delete();             
            }
            else { 
                 \Yii::$app->getSession()->setFlash('error', 'Ошибка при удалении файлов!' ,false );
            }
            
        }
        $this->goHome();
        Yii::$app->end();
    }
    /**
     * Обработка входа пользователя на сайт 
     *
     * @return сгенерированное представление страницы с формой входа
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Обработка выхода пользователя
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    /**
     * вывод представления с графиком
     * наименования данных графика задаются в параметрах,
     * соответствующих ключам массива с данными, по которым он будет построен
     * @param string $id ид отчета в таблице БД
     * @param string $y наименование данных по оси y
     * @param string $x наименование данных по оси x
     * @return сгенерированное представление страницы с графиком
     */

   public function actionView($id,$y='balance',$x='time')
    {
        //поиск модели нужного отчета
        $ReportMmodel = new Reports;
        $ReportMmodel = $this->findModel($id);
        //имя файла с его данными
        $DataFileName = Yii::$app->params['datadir'].'data'.$ReportMmodel->id.'.data';
        //выборка данных с диска в память
        $ReportData = file_get_contents($DataFileName);
        //преобразование в массив из json
        $ReportDataArray = json_decode($ReportData, true);
        //формирование массива с точками графика
        $points = [];
        $xarray = $ReportDataArray[$x];
        $yarray = $ReportDataArray[$y];
        $count=0;
        foreach ($xarray as $row) {
            if((int)$yarray[$count][0] !== 0) {
                $points['x'][] =  $row[0];
                    
                $points['y'][] =  (int)$yarray[$count][0];
            }
            $count++;
        } 
        //создание страницы с графиком 
        return $this->render('view', [
            'points' => $points,         
            'model' => $ReportMmodel
        ]);
        
    }
    /**
     * поиск модели отчета по ее id
     * 
     * @return модель отчета
     */
    protected function findModel($id)
    {
        if (($model = Reports::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрошенная страница не найдена.');
        }
    }
}
