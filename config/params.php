<?php

return [
    'adminEmail' => 'admin@example.com',
    'uploaddir' => $_SERVER['DOCUMENT_ROOT'].'/uploads/',
    'datadir' => $_SERVER['DOCUMENT_ROOT'].'/../data/'
];
