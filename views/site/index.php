<?php
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* Представление главной страницы */
/* @var $this yii\web\View */

$this->title = 'Тестовое задание';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Загрузка файла с данными</h2>
        <!--вывод формы ввода файла -->
<?php $form = ActiveForm::begin(['options' =>[
    'enctype' => 'multipart/form-data', 
    'class' => 'form-horizontal',
    ]
    ]) ?>

    <?= $form->field($model, 'ReportFile')->fileInput(['class' => 'form-control'])->label("") ?>

    <button type="submit" class="btn btn-primary">Загрузить</button>

<?php ActiveForm::end() ?>
    </div>

    <div class="body-content">

        <div class="row">
            <!--вывод таблицы с загруженными файлами -->
            <?php
          Pjax::begin(['id' => 'pjax-container']); 
        echo GridView::widget([
        'dataProvider' => $dataProvider,
                'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                
            'filename',
             [
                'attribute' => 'filename',
                 'label' => 'Ссылка на файл',
                'format' => 'html',    
                'value' => function ($data) {
                    return Html::a('Скачать', Yii::getAlias('@web').'/site/download?id='.$data->id) ;            
                },  
            ],    
                     [
           'class' => \yii\grid\ActionColumn::className(),
           'buttons'=>[
                  'view'=>function ($url, $model) {
                        $customurl=Yii::$app->getUrlManager()->createUrl(['site/view','id'=>$model['id']]); //$model->id для AR
                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                                                ['title' => Yii::t('yii', 'View')]);
               },
                   'delete' => function ($url , $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                        [
                            'data-confirm' => 'Вы уверены, что хотите удалить эту строку?',                          
                            'data-method' =>'POST',              
                            'title' => Yii::t('yii', 'Delete')
                        ] );
                }
            ],
           'template'=>'{view} {delete}',
       ],
            

            
        ],
    ]); 
 
Pjax::end();
?>
            </div>
        </div>

    </div>
