<?php
/* Представление страницы вывода графика */
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Transactions;
use miloschuman\highcharts\Highcharts;
/* @var $this yii\web\View */
/* @var $model app\models\Reports */

$this->title = 'График файла: '. $model->filename;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reports-view">


    <div style="text-align: center;">
     <?php echo $model->reporttitle ;?>
     <?php echo $model->statistic ;?>
    </div>
<br>
<!--вывод таблицы с заголовком -->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        ],
    ]) ?>
<!--вывод графика -->
  <?php  
  
    echo Highcharts::widget([
   'options' => [
           'chart'=>[
        'zoomType'=> 'x'
      ],
      'title'=> [
        'text'=> ''
      ],
       'xAxis' => [
         'categories' => $points['x']
      ],

      'yAxis'=> [
        'title'=> [
          'text'=> 'Баланс'
        ]
      ],
      'legend'=> [
        'enabled'=> false
      ],
      'plotOptions'=> [
        'area'=> [
          'fillColor'=> [
            'linearGradient'=> [
              'x1'=> 0,
              'y1'=> 0,
              'x2'=> 0,
              'y2'=> 1
            ],
            'stops'=> [
            
            ]
          ],
          'marker'=> [
            'radius'=> 2
          ],
          'lineWidth'=> 1,
          'states'=> [
            'hover'=> [
              'lineWidth'=> 1
            ]
          ],
          'threshold'=> null
        ]
      ],

      'series'=> [[
        'type'=> 'area',
        'name'=> 'Баланс',
        'data'=> $points['y']
      ]]
   ]
]);
            
        
            ?>
</div>
