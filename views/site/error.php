<?php
/* Представление страницы ошибкиw */
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        При обработке запроса сервером произошли ошибки, указанные выше на странице.
    </p>
    <p>
        Пожалуйста, обратитесь к нам, если уверены, что это ошибки на стороне сервера.
    </p>

</div>
